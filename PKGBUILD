# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Christoph Haag <haagch@studi.informatik.uni-stuttgart.de>
# Contributor: Laurent Carlier <lordheavym@gmail.com>
# Contributor: Adrià Cereto i Massagué <ssorgatem at gmail.com>
# Contributor: Tomas Kovar <tkov_fedoraproject.org>

# Keep in-sync with https://github.com/tomkv/packaging-rpm/tree/master/amdvlk

pkgname=lib32-amdvlk
amdvlk_commit=245f34b77a49b255d5e419d79fac178fa1c85989
llvm_commit=b9910c8bfcccc63c07c959963fa567120d11f024
llpc_commit=ddb909580e9996356c3bbe23bc1b14c44987eb4c
xgl_commit=96d84068b622b2c3ce8cf9aa8ff597260aa5ad3f
pal_commit=135258ac31901e1293fea5e2f599659ee438ba1f
wsa_commit=f558403d3292039de4d17334e562bda58abfc72c
commit_date=0606
pkgver=2.93.${commit_date}.g${amdvlk_commit:0:7}
pkgrel=1
pkgdesc="AMD's standalone Vulkan driver (32-bit)"
arch=(x86_64)
url="https://github.com/GPUOpen-Drivers"
license=('MIT')
depends=('lib32-vulkan-icd-loader')
provides=('lib32-vulkan-amdvlk' 'lib32-vulkan-driver')
conflicts=('lib32-vulkan-amdvlk')
makedepends=('dri2proto' 'xorg-server-devel' 'cmake' 'python' 'lib32-libxml2' 'lib32-libdrm' 'libxrandr')
source=(AMDVLK-${amdvlk_commit:0:7}.tar.gz::$url/AMDVLK/archive/${amdvlk_commit}.tar.gz
        llvm-${llvm_commit:0:7}.tar.gz::$url/llvm/archive/${llvm_commit}.tar.gz
        llpc-${llpc_commit:0:7}.tar.gz::$url/llpc/archive/${llpc_commit}.tar.gz
        xgl-${xgl_commit:0:7}.tar.gz::$url/xgl/archive/${xgl_commit}.tar.gz
        pal-${pal_commit:0:7}.tar.gz::$url/pal/archive/${pal_commit}.tar.gz
        wsa-${wsa_commit:0:7}.tar.gz::$url/wsa/archive/${wsa_commit}.tar.gz)
sha256sums=('a3ef7b355fbc214c397f64a7add6ad856421020ab1429ec58a507d3bee35a704'
            '8b0052682f0665e18ea3f4b7a9b533dce95949c366c0d82a9e388056706383ae'
            '32e6f6e330a7ff82bd7840e43a81d7950878c9eae93f0e1e376c37c27426c244'
            '49f6cfd63f89db1a2cbf1bb5be882919af45443e057efb1715785e0983b3cd32'
            'dfebaf29c6dedb9c6fcbe4d02407a4267703aa7ae570d105ef146f2df455486f'
            'b23e9453fa7b14bb13157fb645936ec74b18b12cdef301758452a92b23f27705')

prepare() {
  ln -s AMDVLK-${amdvlk_commit} AMDVLK
  ln -s llvm-${llvm_commit} llvm
  ln -s llpc-${llpc_commit} llpc
  ln -s xgl-${xgl_commit} xgl
  ln -s pal-${pal_commit} pal
  ln -s wsa-${wsa_commit} wsa

  # Disable -Werror, https://github.com/GPUOpen-Drivers/AMDVLK/issues/89
  for i in xgl/icd/CMakeLists.txt llpc/CMakeLists.txt llpc/imported/metrohash/CMakeLists.txt llvm/utils/benchmark/CMakeLists.txt llvm/utils/benchmark/test/CMakeLists.txt pal/src/core/imported/addrlib/CMakeLists.txt pal/src/core/imported/vam/CMakeLists.txt pal/shared/gpuopen/cmake/AMD.cmake
  do
    sed -i "s/-Werror//g" "$srcdir"/$i
  done
}

build() {
  msg "building xgl..."
  cd xgl
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  cmake -H. -Bbuilds/Release \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_FLAGS=-m32 \
    -DCMAKE_CXX_FLAGS=-m32 \
    -DLLVM_TARGET_ARCH:STRING=i686 \
    -DLLVM_DEFAULT_TARGET_TRIPLE="i686-pc-linux-gnu"

  cd builds/Release
  make
  msg "building xgl finished!"
}

package() {
  install -m755 -d "${pkgdir}"/usr/lib32
  install -m755 -d "${pkgdir}"/usr/share/vulkan/icd.d
  install -m755 -d "${pkgdir}"/usr/share/licenses/lib32-amdvlk

  install xgl/builds/Release/icd/amdvlk32.so "${pkgdir}"/usr/lib32/
  install AMDVLK/json/Redhat/amd_icd32.json "${pkgdir}"/usr/share/vulkan/icd.d/
  install AMDVLK/LICENSE.txt "${pkgdir}"/usr/share/licenses/lib32-amdvlk/

  sed -i "s/\/lib/\/lib32/g" "${pkgdir}"/usr/share/vulkan/icd.d/amd_icd32.json
}
